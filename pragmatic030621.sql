-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 04:04 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pragmatic030621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/02 16:45:43', '2021-06-02', '2021-06-02', 1, 'Abbott'),
(2, 'Dr Mahadev Swamy ', 'drbms83@gmail.com', 'Bangalore', 'Chinmaya Hrudayalaya', NULL, NULL, '2021/06/03 15:04:35', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(3, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/06/03 18:04:44', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(4, 'Dr Mahadev Swamy ', 'drbms83@gmail.com', 'BANGALORE', 'Chinmaya Hrudayalaya', NULL, NULL, '2021/06/03 18:36:50', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(5, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/06/03 18:40:52', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(6, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/06/03 18:43:52', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(7, 'Yogesh Kothari ', 'dryogeshkothari@gmail.com', 'bangalore', 'Bhagwan Mahaveer Hospital ', NULL, NULL, '2021/06/03 18:49:30', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(8, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/06/03 18:50:17', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(9, 'Yogesh Kothari ', 'dryogeshkothari@gmail.com', 'Bangalore', 'Bhagwan Mahaveer Jain Hospital', NULL, NULL, '2021/06/03 18:51:10', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(10, 'Dr. Navin Mathew', 'nischalnh8825@gmail.com', 'Kochi', 'Amrita Institute Of Medical Sciences', NULL, NULL, '2021/06/03 18:51:45', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(11, 'Jimmy George', 'pjimmygeorge@gmail.com', 'Ernakulam', 'Lisie ', NULL, NULL, '2021/06/03 18:53:45', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(12, 'Dr Mahadev Swamy ', 'drbms83@gmail.com', 'BANGALORE', 'Chinmaya Hrudayalaya', NULL, NULL, '2021/06/03 18:54:36', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(13, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021/06/03 18:54:47', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(14, 'prabhakara shetty Heggunje', 'pheggunje@gmail.com', 'Bangalore', 'coulumbia Asia hospital', NULL, NULL, '2021/06/03 18:58:35', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(15, 'Dr Sreekanth Shetty', 'sreekanth_shetty@yahoo.com', 'Benagaluru', 'Sakra', NULL, NULL, '2021/06/03 19:02:00', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(16, 'Ameet sattur', 'satturag@gmail.com', 'hubballi', 'KLE-Suchirayu', NULL, NULL, '2021/06/03 19:02:54', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(17, 'Ameet G Sattur', 'satturag@gmail.com', 'Hubli', 'KLE-Suchirayu', NULL, NULL, '2021/06/03 19:10:06', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(18, 'Dr Mahadev Swamy ', 'drbms83@gmail.com', 'BANGALORE', 'Chinmaya Hrudayalaya', NULL, NULL, '2021/06/03 19:13:38', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(19, 'Dr Mahadev Swamy ', 'drbms83@gmail.com', 'BANGALORE', 'Chinmaya Hrudayalaya', NULL, NULL, '2021/06/03 19:26:02', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(20, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Hosur', 'ABBOTT', NULL, NULL, '2021/06/03 19:52:40', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(21, 'Sreekanth Shetty', 'sreekanthshetty1974@gmail.com', 'Bengaluru ', 'Sakra', NULL, NULL, '2021/06/03 20:09:31', '2021-06-03', '2021-06-03', 1, 'Abbott'),
(22, 'Subhabrata', 'subhabrata.banerjee@abbott.com', 'kolkata', 'av', NULL, NULL, '2021/06/05 11:31:44', '2021-06-05', '2021-06-05', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'KARTHIK VASUDEVAN', 'karvasudevan@icloud.com', 'It looks like wire induced dissection. well managed ', '2021-06-03 20:04:40', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-02 16:50:43', '2021-06-02 16:50:43', '2021-06-02 16:50:53', 0, 'Abbott', '92596112cb8ce58f00232639ee506232'),
(2, 'Kalyan Goswami', 'Kalyan61@yahoo.co.in', 'Bangalore ', 'AV ', NULL, NULL, '2021-06-02 21:26:27', '2021-06-02 21:26:27', '2021-06-02 22:56:27', 0, 'Abbott', '5e2ef506a6476be72afe4520bed473e3'),
(3, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-02 22:19:11', '2021-06-02 22:19:11', '2021-06-02 23:49:11', 0, 'Abbott', '071df3f2bd60ea7a1268de586ab51b3f'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-02 22:47:14', '2021-06-02 22:47:14', '2021-06-02 22:47:22', 0, 'Abbott', '04b18e2f51694938ad43defe12fbc10b'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-02 22:48:48', '2021-06-02 22:48:48', '2021-06-02 22:48:58', 0, 'Abbott', 'e3a3cef0206b999a88f1a4e07a282572'),
(6, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bangalore', NULL, NULL, '2021-06-02 22:51:47', '2021-06-02 22:51:47', '2021-06-02 22:51:52', 0, 'Abbott', '9119335bb280708be608f23c98274df7'),
(7, 'Utsav Unadkat', 'dr.utsavunadkat@gmail.com', 'Ahmedabad ', 'STAR Hospital ', NULL, NULL, '2021-06-03 12:03:40', '2021-06-03 12:03:40', '2021-06-03 13:33:40', 0, 'Abbott', 'e34f8fe90c9501922a288cb3b71ee5b8'),
(8, 'Keshav Prabhu', 'keshavprabhu.70@gmail.com', 'Bengaluru', 'Chinmaya Hrudayalaya', NULL, NULL, '2021-06-03 17:10:27', '2021-06-03 17:10:27', '2021-06-03 18:40:27', 0, 'Abbott', 'ab1a5862d3a0b38dfb726f33e4fefe4e'),
(9, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-03 17:31:17', '2021-06-03 17:31:17', '2021-06-03 17:31:25', 0, 'Abbott', 'e3858af457ed200a89c3de4e2377570b'),
(10, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-03 17:33:12', '2021-06-03 17:33:12', '2021-06-03 19:03:12', 0, 'Abbott', 'febb06575e594e4f834e52c6720ba672'),
(11, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-03 17:38:25', '2021-06-03 17:38:25', '2021-06-03 17:39:01', 0, 'Abbott', '1c0db35fecee38bf10d8ae4ce7b4004c'),
(12, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-03 17:58:11', '2021-06-03 17:58:11', '2021-06-03 19:28:11', 0, 'Abbott', '0692cd2b751e494a2f2bf7e63e39a95d'),
(13, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021-06-03 18:43:11', '2021-06-03 18:43:11', '2021-06-03 20:13:11', 0, 'Abbott', 'f1a08b4004efe655a1fdcd57131e5400'),
(14, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-03 18:44:39', '2021-06-03 18:44:39', '2021-06-03 20:14:39', 0, 'Abbott', '2f4ccd03eee5e7afc16a7903e3cc34b9'),
(15, 'Dr ravi', 'drravicardio@gmail.com', 'Bangalore ', 'Bgs global hospital', NULL, NULL, '2021-06-03 18:45:07', '2021-06-03 18:45:07', '2021-06-03 20:15:07', 0, 'Abbott', 'eb48a7f374548e44b371b2d7b572b5b8'),
(16, 'Lijo ', 'lijo.k@abbott', 'Cochin', 'Abbott', NULL, NULL, '2021-06-03 18:56:27', '2021-06-03 18:56:27', '2021-06-03 20:26:27', 0, 'Abbott', '556d7a8c0f6b8a9a5555f730e4250a3d'),
(17, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-03 18:57:14', '2021-06-03 18:57:14', '2021-06-03 20:27:14', 0, 'Abbott', 'df78c096a39420b65a8e53330e31e912'),
(18, 'Keshav Prabhu', 'keshavprabhu.70@gmail.com', 'Bengaluru', 'Chinmaya Hrudayalaya', NULL, NULL, '2021-06-03 19:00:15', '2021-06-03 19:00:15', '2021-06-03 20:30:15', 0, 'Abbott', 'ff63e84d195e755eef59c4d459c772d2'),
(19, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-03 19:01:40', '2021-06-03 19:01:40', '2021-06-03 20:31:40', 0, 'Abbott', '0160f32b400ffaaaee513502f1c71d57'),
(20, 'Kalyan Goswami ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-06-03 19:01:48', '2021-06-03 19:01:48', '2021-06-03 20:31:48', 0, 'Abbott', '7adb02e645073cc4fa5b19cc60faca39'),
(21, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-06-03 19:02:20', '2021-06-03 19:02:20', '2021-06-03 20:32:20', 0, 'Abbott', '1544a834cc1f78168717821a49224459'),
(22, 'Lijo ', 'lijo.k@abbott.com', 'Cochin', 'Abbott ', NULL, NULL, '2021-06-03 19:03:06', '2021-06-03 19:03:06', '2021-06-03 20:33:06', 0, 'Abbott', '6df534bf41313ef8ebed7157f205ff41'),
(23, 'Dr ravi', 'dreavicardio@gmail.com', 'Bangalore', 'Bgs', NULL, NULL, '2021-06-03 19:03:52', '2021-06-03 19:03:52', '2021-06-03 19:04:06', 0, 'Abbott', '78722c31c9ad202d56f1ebafaddd8b9c'),
(24, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-06-03 19:05:34', '2021-06-03 19:05:34', '2021-06-03 20:35:34', 0, 'Abbott', '48abf7d998898f84043052de90dfd7bf'),
(25, 'sridhar', 'drnsridhara@gmail.com', 'bangalore', 'fortis', NULL, NULL, '2021-06-03 19:06:15', '2021-06-03 19:06:15', '2021-06-03 20:36:15', 0, 'Abbott', '43d515d9f314ea78b9f49d2188f33b59'),
(26, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-03 19:06:27', '2021-06-03 19:06:27', '2021-06-03 20:30:23', 0, 'Abbott', 'a18ed3df809aa08f3fd1f77062a0f34a'),
(27, 'Sharad Masudi', 'sharad.masudi@gmail.com', 'Bengaluru', 'Sri Jayadeva institute of Cardiology', NULL, NULL, '2021-06-03 19:06:28', '2021-06-03 19:06:28', '2021-06-03 20:36:28', 0, 'Abbott', '4558f097637769fa7bf5de9fd65cb76b'),
(28, 'ravi', 'ravismath76@gmail.com', 'bangalore', 'jayadeva hospital', NULL, NULL, '2021-06-03 19:07:04', '2021-06-03 19:07:04', '2021-06-03 20:37:04', 0, 'Abbott', '834ed5f80f4b000b98af5d8b7976ff7b'),
(29, 'ANKIT SINGH ', 'ankitsingh0612@gmail.com', 'Bengaluru', 'Sri jayadeva institute of cardiovascular sciences and research ', NULL, NULL, '2021-06-03 19:07:07', '2021-06-03 19:07:07', '2021-06-03 20:37:07', 0, 'Abbott', '60472507e81b0c6e1797877b59a0da3d'),
(30, 'sreedhara ', 'kummas82@gmail.com', 'mysore ', 'sjicr ', NULL, NULL, '2021-06-03 19:07:16', '2021-06-03 19:07:16', '2021-06-03 20:37:16', 0, 'Abbott', '6fa3a6c4ccf5b02b41e6f87ebb503921'),
(31, 'Naveen', 'naveenstella@gmail.com', 'Bangalore ', 'Apollo', NULL, NULL, '2021-06-03 19:10:34', '2021-06-03 19:10:34', '2021-06-03 20:40:34', 0, 'Abbott', '5c44ff4fa11d136126487413e90ede3c'),
(32, 'Priyotosh Bhuniya', 'priyotoshbhuniya@gmail.com', 'Jamshedpur ', 'Tata Main Hospital', NULL, NULL, '2021-06-03 19:12:19', '2021-06-03 19:12:19', '2021-06-03 20:42:19', 0, 'Abbott', 'edf87d65f990607f97f4384e1cd61b6a'),
(33, 'Sridhar', 'sridharag@hotmail.com', 'Bangalore ', 'Manipal', NULL, NULL, '2021-06-03 19:13:33', '2021-06-03 19:13:33', '2021-06-03 20:43:33', 0, 'Abbott', 'a8fd9ce0c8f2f14601f315f461c172da'),
(34, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi ', 'Abbot ', NULL, NULL, '2021-06-03 19:15:02', '2021-06-03 19:15:02', '2021-06-03 20:45:02', 0, 'Abbott', 'a0dbed1875df64d1dc8305942be35b8c'),
(35, 'Keshav Prabhu', 'keshavprabhu.70@gmail.com', 'Bengaluru', 'Chinmaya Hrudayalaya', NULL, NULL, '2021-06-03 19:15:03', '2021-06-03 19:15:03', '2021-06-03 20:45:03', 0, 'Abbott', '9568c7f39bd1a8442891e62f2ce90c92'),
(36, 'Dr ravi', 'drravicardio@gmail.com', 'Bangalore ', 'Bgs', NULL, NULL, '2021-06-03 19:16:50', '2021-06-03 19:16:50', '2021-06-03 20:46:50', 0, 'Abbott', 'e157a6f93d2ef8a9add11f2df5ec3272'),
(37, 'Dr ravi', 'drravicardio@gmail.com', 'Bangalore ', 'Bgs', NULL, NULL, '2021-06-03 19:17:55', '2021-06-03 19:17:55', '2021-06-03 20:47:55', 0, 'Abbott', '047069bee26045542e5160a576b98473'),
(38, 'Shyam', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-03 19:23:03', '2021-06-03 19:23:03', '2021-06-03 20:53:03', 0, 'Abbott', '7907b9add7ae8a987e64c5ec0d1a0b28'),
(39, 'Mahendra ', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-06-03 19:23:39', '2021-06-03 19:23:39', '2021-06-03 20:53:39', 0, 'Abbott', 'bda564e8a95a8cecc77b3a3e5e973412'),
(40, 'Chaitanya Vadlapatla', 'chaitanya.vadlapatlas@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-06-03 19:32:15', '2021-06-03 19:32:15', '2021-06-03 21:02:15', 0, 'Abbott', 'bf3ca645d3ff1c21c732f882f1909c05'),
(41, 'R S VENKATESULU ', 'rsv_vhcb@yahoo.com', 'Bangalore ', 'Aster CMI ', NULL, NULL, '2021-06-03 19:32:19', '2021-06-03 19:32:19', '2021-06-03 21:02:19', 0, 'Abbott', '894455e54d0f6baf369c3f95f3bb2ef8'),
(42, 'Yogesh', 'patil.yogesh15@gmail.com', 'Bangalore', 'Manipal', NULL, NULL, '2021-06-03 19:32:32', '2021-06-03 19:32:32', '2021-06-03 21:02:32', 0, 'Abbott', 'c7b00446412726077f061844d4f0bfd2'),
(43, 'Aniketh Vijay', 'aniketh.vijay7@gmail.com', 'Chikmagalur ', 'Ashraya heart institute ', NULL, NULL, '2021-06-03 19:34:39', '2021-06-03 19:34:39', '2021-06-03 21:04:39', 0, 'Abbott', '05166cdc7f487877498026dadf2ebfd8'),
(44, 'Aniketh Vijay', 'aniketh.vijay7@gmail.com', 'Chikmagalur ', 'Ashraya heart institute ', NULL, NULL, '2021-06-03 19:38:19', '2021-06-03 19:38:19', '2021-06-03 21:08:19', 0, 'Abbott', 'd57a4cfbe2b5ec1cb7de6061908d56d8'),
(45, 'Prabhavathi Enter Last Name', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-06-03 19:38:34', '2021-06-03 19:38:34', '2021-06-03 21:08:34', 0, 'Abbott', 'a9b785a3d9536f6238bad401e7d15427'),
(46, 'Afsar ', 'afsar737@gmail.com', 'Bangalore ', 'Manipal hospital ', NULL, NULL, '2021-06-03 19:38:51', '2021-06-03 19:38:51', '2021-06-03 21:08:51', 0, 'Abbott', '4c8a40a1d5aacfd44624f13fa3f7a8f9'),
(47, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbot ', NULL, NULL, '2021-06-03 19:39:44', '2021-06-03 19:39:44', '2021-06-03 21:09:44', 0, 'Abbott', 'a024bccd40b0dbb21c161457f82c2557'),
(48, 'Padmakumar ', 'padmakumar69@yahoo.co.in', 'Manipal ', 'Kmc', NULL, NULL, '2021-06-03 19:43:19', '2021-06-03 19:43:19', '2021-06-03 21:13:19', 0, 'Abbott', 'afc1adbdb56dc8e4a2fad496c70b7889'),
(49, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-06-03 19:47:14', '2021-06-03 19:47:14', '2021-06-03 21:17:14', 0, 'Abbott', '7593cd45a8b550e69c7cfeca9892a47c'),
(50, 'KARTHIK VASUDEVAN', 'karvasudevan@icloud.com', 'BANGALORE, INDIA', 'ColumbiaAsia Hospital', NULL, NULL, '2021-06-03 19:47:50', '2021-06-03 19:47:50', '2021-06-03 21:17:50', 0, 'Abbott', '14f04f2d4895952d4eaed4d11223d65b'),
(51, 'KHS', 'drkhsrinivas@yahoo.co.in', 'Bangalore ', 'SJICR', NULL, NULL, '2021-06-03 19:58:18', '2021-06-03 19:58:18', '2021-06-03 21:28:18', 0, 'Abbott', '6c3092819e84968d24c04f298214ba92'),
(52, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-03 20:00:09', '2021-06-03 20:00:09', '2021-06-03 21:30:09', 0, 'Abbott', 'bbb4267848e78f7972509a1800f07b60'),
(53, 'Prabhavathi Enter Last Name', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-06-03 20:05:21', '2021-06-03 20:05:21', '2021-06-03 21:35:21', 0, 'Abbott', '0a7b8664a21e1ca2da70f2139d3a6ac6'),
(54, 'Sridhara ', 'sgopalasastry@gmail.com', 'Bangalore ', 'Vikram', NULL, NULL, '2021-06-03 20:05:42', '2021-06-03 20:05:42', '2021-06-03 21:35:42', 0, 'Abbott', '6c289d77ab9e6069e282dbe2a9ddf211'),
(55, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-06-03 20:07:16', '2021-06-03 20:07:16', '2021-06-03 21:37:16', 0, 'Abbott', '0c25b9504739d715875521e02c1e213e'),
(56, 'Dr Darshan', 'drdoc2002@gmail.com', 'Bangalore', 'Manipal bangalore', NULL, NULL, '2021-06-03 20:16:17', '2021-06-03 20:16:17', '2021-06-03 20:30:35', 0, 'Abbott', 'a75b7c305cc3bebcc9caee25b43eae8c'),
(57, 'Kalyan Goswami ', 'kalyan.goswami@abbott.com', 'Bangalore ', 'AV', NULL, NULL, '2021-06-03 20:19:32', '2021-06-03 20:19:32', '2021-06-03 20:30:33', 0, 'Abbott', '2379b2de7228a316734bfb1e65c3baf1'),
(58, 'Sharad Masudi', 'sharad.masudi@gmail.com', 'Bidar', 'Sri Jayadeva institute of Cardiology', NULL, NULL, '2021-06-03 20:21:23', '2021-06-03 20:21:23', '2021-06-03 21:51:23', 0, 'Abbott', 'b3dfc346611f37e002ef9d5c8be4cae6'),
(59, 'Keshav Prabhu', 'keshavprabhu.70@gmail.com', 'Bengaluru', 'Chinmaya Hrudayalaya', NULL, NULL, '2021-06-03 20:25:02', '2021-06-03 20:25:02', '2021-06-03 20:30:41', 0, 'Abbott', '17e4078a8cfdc4adb2334be0cf3895f3'),
(60, 'Yogesh', 'patil.yogesh15@gmail.com', 'Bangalore', 'Manipal', NULL, NULL, '2021-06-03 20:27:31', '2021-06-03 20:27:31', '2021-06-03 20:30:10', 0, 'Abbott', '011315fff6551760aaea8eee36261022'),
(61, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-06-03 20:29:03', '2021-06-03 20:29:03', '2021-06-03 21:59:03', 0, 'Abbott', 'dfd9f3896417367c3333ffacc672c332'),
(62, 'aj swamy', 'ajayswamy@rediffmail.com', 'bangalore', 'command hospital', NULL, NULL, '2021-06-03 20:41:23', '2021-06-03 20:41:23', '2021-06-03 22:11:23', 0, 'Abbott', '89f29c38d9289b301bee53d46268a061'),
(63, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'Y', NULL, NULL, '2021-06-03 20:55:14', '2021-06-03 20:55:14', '2021-06-03 22:25:14', 0, 'Abbott', '0b4a476be1039149c357a8bf4cc54c7a'),
(64, 'MSUDHAKAR RAO', 'msudhakar88@gmail.com', 'MANIPAL', 'Kmc', NULL, NULL, '2021-06-03 21:03:00', '2021-06-03 21:03:00', '2021-06-03 22:33:00', 0, 'Abbott', '4dbd8efb8d6a89417a0d002d20562908'),
(65, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita hospital', NULL, NULL, '2021-06-03 22:03:44', '2021-06-03 22:03:44', '2021-06-03 23:33:44', 0, 'Abbott', '77729e7a9a786c5fff6eb9f81899034e'),
(66, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'T', NULL, NULL, '2021-06-04 19:06:54', '2021-06-04 19:06:54', '2021-06-04 20:36:54', 0, 'Abbott', '1562a18e13e7aa03767d63454229427e'),
(67, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita', NULL, NULL, '2021-06-10 18:52:57', '2021-06-10 18:52:57', '2021-06-10 20:22:57', 1, 'Abbott', 'd2d17c561bcc4c0cf2cd522766cddedf'),
(68, 'Abdul Rahman', 'drabdulr1@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-06-10 19:11:36', '2021-06-10 19:11:36', '2021-06-10 20:41:36', 1, 'Abbott', 'b87d053be7c837f4db553be3ffc60a61'),
(69, 'Veena Nanjappa', 'veenananjappa@yahoo.co.in', 'Mysore', 'Sjicsr', NULL, NULL, '2021-06-15 07:31:22', '2021-06-15 07:31:22', '2021-06-15 09:01:22', 1, 'Abbott', '593d9660c5f804f88db01f4092696db3'),
(70, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-06-21 19:14:38', '2021-06-21 19:14:38', '2021-06-21 20:44:38', 1, 'Abbott', 'b7348579ecd10293c81371cdb162ad14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
